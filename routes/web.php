<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [HomeController::class , 'index'])->name('index');
Route::get('/register', [AuthController::class , 'register'])->name('register');
Route::get('/welcome', [AuthController::class , 'welcome'])->name('welcome');

//Cast
Route::get('/cast', [CastController::class , 'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class , 'create'])->name('cast.create');
Route::post('/cast', [CastController::class , 'store'])->name('cast.store');
Route::get('/cast/{cast_id}', [CastController::class , 'show'])->name('cast.show');
Route::get('/cast/{cast_id}/edit', [CastController::class , 'edit'])->name('cast.edit');
Route::put('/cast/{cast_id}', [CastController::class , 'update'])->name('cast.update');
Route::delete('/cast/{cast_id}', [CastController::class , 'destroy'])->name('cast.destroy');

//table latihan
Route::get('/table', function() {
    return view('tables.table');
})->name('tables.table');
Route::get('/data-tables', function() {
    return view('tables.data-tables');
})->name('tables.data-tables');

