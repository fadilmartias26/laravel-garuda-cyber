@extends('layouts.master')
@section('title', 'Cast')
@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{ URL::asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-end">
                        <a href="{{ route('cast.create') }}" class="btn btn-primary">Tambah Cast</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width:10px">#</th>
                                    <th>Nama</th>
                                    <th>Umur</th>
                                    <th>Bio</th>
                                    <th style="width:50px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cast as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->nama }}</td>
                                    <td>{{ $value->umur }}</td>
                                    <td>{{ $value->bio }}</td>
                                    <td class="d-flex">
                                        <div class="mr-2">
                                            <a href="{{ route('cast.show', $value->id) }}" class="btn btn-secondary">Lihat</a>
                                        </div>
                                        <div class="mr-2">
                                            <a href="{{ route('cast.edit', $value->id) }}" class="btn btn-warning">Edit</a>
                                        </div>
                                        <div>
                                            <form action="{{ route('cast.destroy', $value->id) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
