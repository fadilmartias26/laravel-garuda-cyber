@extends('layouts.master')
@section('title', 'Detail Cast')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <table>
                            <tr>
                                <th>Nama&emsp;: </th>
                                <td>{{ $cast->nama }}</td>
                            </tr>
                            <tr>
                                <th>Umur&emsp;: </th>
                                <td>{{ $cast->umur }}</td>
                            </tr>
                            <tr>
                                <th>Bio&emsp;&emsp;: </th>
                                <td>{{ $cast->bio }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endsection
