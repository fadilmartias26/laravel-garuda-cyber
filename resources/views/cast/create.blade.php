@extends('layouts.master')
@section('title', 'Tambah Cast')
@section('content')
<div class="container-fluid">
    <div class="row">
        <!-- left column -->
        <div class="col">
            <!-- general form elements -->
            <div class="card">
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('cast.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Email" required>
                        </div>
                        @error('bio')
                            {{ $message }}
                            @enderror
                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" id="umur"
                                placeholder="Umur" required>
                        </div>
                        @error('bio')
                            {{ $message }}
                            @enderror
                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <textarea type="email" class="form-control @error('bio') is-invalid @enderror" id="bio" name="bio" placeholder="Bio" required></textarea>
                        </div>
                        @error('bio')
                            {{ $message }}
                            @enderror
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
