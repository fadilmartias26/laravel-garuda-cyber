<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <div>
            <label for="f_name">Nama Depan :</label> <br>
            <input type="text" id="f_name" name="f_name">
        </div> <br>
        <div>
            <label for="l_name">Nama Belakang :</label> <br>
            <input type="text" id="l_name" name="l_name">
        </div> <br>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
