<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));

    }

    public function create()
    {
        return view('cast.create');

    }

    public function store(Request $request)
    {
       $data = $request->validate([
        'nama' => 'required|string',
        'umur' => 'required|integer',
        'bio' => 'required|string',
       ]);

       DB::table('cast')->insert([
        'nama' => $data['nama'],
        'umur' => $data['umur'],
        'bio' => $data['bio'],
       ]);

       return redirect()->route('cast.index');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
       $cast = DB::table('cast')->where('id', $id)->first();

       return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nama' => 'required|string',
            'umur' => 'required|integer',
            'bio' => 'required|string',
        ]);

        $cast = DB::table('cast')->where('id', $id)->update([
            'nama' => $data['nama'],
            'umur' => $data['umur'],
            'bio' => $data['bio'],
        ]);

        return redirect()->route('cast.index');
    }

    public function destroy($id)
    {
        $cast = DB::table('cast')->where('id', $id)->delete();

        return redirect()->route('cast.index');
    }
}
